# Greed grid parameters

If you want to modify the grid parameters, modify the variables in your grid container selector for instance.
```css
.greed {
  --min-column-width: 300px; /* default value */
  --max-column-width: 1fr; /* default value */
  --nb-column: 4; /* default value */
  --grid-gap: 50px; /* default value */
  /* ... */
}
```
